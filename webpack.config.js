'use strict'; // eslint-disable-line strict
console.log("Webpack dev");
const path = require('path');
const webpack = require('webpack');

module.exports = {
  devtool: 'eval-source-map',
  entry: [
    './src/common.js'
  ],
  output: {
    path: path.join(__dirname, 'public'),
    filename: 'index.js',
  },
  plugins: [
    new webpack.NoErrorsPlugin(), // запрещает записывание скриптов с ошибками
	 new webpack.DefinePlugin({     // свои переменные в скриптах
		  cutCode: JSON.stringify(true)
	  })
  ],
  module: {
	  rules: [
		  { test: /\.js$/,
           exclude: /node_modules/,
           loader: "babel-loader" }
	  ]
  }
};
