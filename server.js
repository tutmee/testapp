const express            = require('express');
const logger             = require('morgan');
const bodyParser         = require('body-parser');
const http               = require('http');
const config             = require('./config/index.js');

const app = express();

app.set("port", config.get("port"))
http.createServer(app).listen(config.get("port"), function(){
	console.log("Express listen");
});

// app.use(logger("combined"));


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static('public'));




