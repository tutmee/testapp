import React, { Component, PropTypes } from 'react'
import { Field, reduxForm } from 'redux-form';



const required = value => value ? undefined : 'Required';
const minLength = min => value =>
    value && value.length < min ? `Must be ${min} characters or more` : undefined;
const minLength6 = minLength(6);

const email = value =>
	value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value) ?
		'Invalid email address' : undefined;


const validate = (values) => {
    const errors = {};
   if( values.password !== values.confirmPassword){

        errors.confirmPassword = "Dismiss"
   }
   return errors
};





const renderField = ({ input, label, type, meta: { touched, error, warning } }) => (
   <div>
      <label className={error != "Required" ? 'step1-label error' :'step1-label' }> {(error !== "Required") ? error : label}</label>
      <div>
         <input {...input} type={type}/>

      </div>
   </div>
);


class AppForm extends Component {
   
  constructor(props){
      super(props);
      this.state = {
          password:'state'
      };
      this.savePassword = this.savePassword.bind(this);
      this.matchPassword = this.matchPassword.bind(this)
  }
  savePassword(e){

        }
  matchPassword(e){
    }
  render() {
    const {handleSubmit, submitting} = this.props;

    return (
      <div className='AppForm'>
        <div className="title">
           <h1>Sign up</h1>
           <div className="progress-bar step1"></div>
        </div>
        <div>
                  <form onSubmit={handleSubmit}>
                     <div className="input input-email">
                        <Field name="email" type="email"
                               component={renderField}
                               label="Email is required"
                               validate={[required, email]}
                        />
                     </div>
                     <div className="input input-password">
                        <Field name="password"
                               component={renderField}
                               label="Password"
                               validate={[required, minLength6]}
                               type="password"
                               onBlur={(e) =>{this.savePassword(e)}}
                        />
                     </div>
                     <div className="input input-password">
                        <Field name="confirmPassword"
                               component={renderField}
                               label="Confirm Password"
                               onBlur={(e) => {this.matchPassword(e)}}
                               validate={[required, minLength6]}
                               type="password"
                        />
                     </div>
                      <div className="footer">
                        <button className="btn-step1" disabled={submitting}>Next</button>
                      </div>
                  </form>
        </div>
      </div>
    )
  }
}



AppForm = reduxForm({
    form: 'test-form',
    destroyOnUnmount: false,
    validate
})(AppForm);


export default AppForm;
