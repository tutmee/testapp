import AppForm from './App';
import Step2 from './Step2';
import React, { Component } from 'react';
import { connect } from 'react-redux'
import saveValuesStep1 from "../actions/saveValueStep1";
import saveValuesStep2 from "../actions/saveValueStep2";
import Dashboard from './Dashboard'

class Form extends Component {

	constructor(props) {
		super(props);
		this.views = {
      		form1: 'form1',
      		form2: 'form2',
      		form3: 'form3',
		};
		this.state = {
			view: 'form1'
		};
		this.displayForm = this.displayForm.bind(this);

	}

	submit(values){
		console.log(values);
		this.props.saveValuesStep1(values);
		this.displayForm(this.views.form2)

	}
  	submit2(values){
    	console.log(values);
    	this.props.saveValuesStep2(values);
		this.displayForm(this.views.form3)
  }

  	displayForm(newView){
  		this.setState({
			view:newView
		})
	}

	render() {
		switch (this.state.view) {
			case this.views.form1:
        return (

					<AppForm onSubmit={this.submit.bind(this)} />
        );
			case this.views.form2:
		return (
					<Step2 		 back={() => {this.setState({view: this.views.form1})}}
								 onSubmit={this.submit2.bind(this)} />
				);
      		case this.views.form3:
      	return (
					<Dashboard back={() => {this.setState({view: this.views.form2})}} />
      		  )
		}
	}
}






Form = connect(() => ({}), {
  saveValuesStep1: saveValuesStep1,
  saveValuesStep2: saveValuesStep2,
})(Form);
export default Form;
