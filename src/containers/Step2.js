import React, { Component } from 'react'
// import { connect } from 'react-redux'
import Step2 from '../components/Step2'
import { BrowserRouter as Router, Route,  Link } from 'react-router-dom'
import { IndexRoute, Switch } from 'react-router'
import Dashboard from './Dashboard'


export default class Step2Container extends Component {

  render() {

    return (
      <div className='List'>
        <div className='Row'>
                <Step2 onSubmit={this.props.onSubmit}
                       back={this.props.back}
                       displayForm={this.props.displayForm}/>
        </div>
      </div>
    )
  }
}
