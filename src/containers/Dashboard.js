import React, { Component } from 'react'
import { connect } from 'react-redux'
import Dashboard from '../components/Dashboard'

class DashboardContainer extends Component {

	render() {
		return (
			<div className='dashboard-container'>
				<Dashboard bla="Bla-bla-bla" back={this.props.back}/>
			</div>
		)
	}
}

const mapStateToProps = (state) => {
	return {
		displayValue: state.saveValue
	}
};

DashboardContainer = connect(mapStateToProps)(Dashboard);
export default DashboardContainer;
