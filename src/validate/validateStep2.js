const validateStep2 = values => {
		const errors = {};
		let day = values.day;
		let month = values.month;
		let year = values.year;
		let age = 18;
		let mydate = new Date();
		mydate.setFullYear(year, month, day);
		let currdate = new Date();
		currdate.setFullYear(currdate.getFullYear() - age);
		if ((currdate - mydate) < 0){
			errors.year = "You must be older 18"
		}

	return errors
};

export default validateStep2;