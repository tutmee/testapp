import React from 'react';
import { render } from 'react-dom';
import { BrowserRouter as Router, Route,  Link} from 'react-router-dom';
import { IndexRoute, Switch, Redirect } from 'react-router';
import { Field, reduxForm } from 'redux-form';
import { Provider } from 'react-redux';
import Form from './containers/Form'




import store from './reducers/Index'




render(
	<Provider store={store}>
		<Router>
			<div className="app">
				<Route path="/" component={Form} />
			</div>
		</Router>
	</Provider>,
	document.getElementById('root')
);
