
const saveValue = (state = {}, actions) => {
	switch (actions.type) {
		case "step1": return Object.assign({}, state, actions.valueStep1);
			break;
		case "step2": return Object.assign({}, state, actions.valueStep2);
			break;
		default: return state
	}
};

export default saveValue;
