import { createStore, combineReducers } from 'redux'
import { reducer as formReducer }       from 'redux-form'

import saveValue from './saveValue'
import account from './account'


const reducers = {
	form: formReducer,
	saveValue: saveValue,
	account:account
};
const reducer = combineReducers(reducers);
const store   = createStore(
	reducer,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);


export default store
