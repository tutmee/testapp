import React, { Component } from 'react'
import { Field, reduxForm } from 'redux-form';


export default class Dashboard extends Component {

	constructor(props) {
		super(props);
		this.displayValueForm = this.displayValueForm.bind(this);
		this.value = this.props.displayValue;
	}
	displayValueForm(e){
		e.preventDefault();
		console.log(this.value);
	}

	render() {

		return (
			<div className='dashboard'>
				<div className="title">
					<h1>Thank you!</h1>
					<div className="progress-bar full"></div>
				</div>
				<div className="wrap-img">
					<img src="images/circle.png" alt="circle	" />
				</div>
				<button onClick={(e)=>{this.displayValueForm(e)}}>Go to Dashboard!</button>
			</div>
		);
	}
}
