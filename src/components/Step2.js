import React, { Component } from 'react'
import {reduxForm, Field} from 'redux-form'
import { connect } from 'react-redux'

import validate from '../validate/validateStep2'


const required = value => value ? undefined : 'Required';

const number = value => value && isNaN(Number(value)) ? 'Must be a number' : undefined;

const minValue = min => value =>
    value && value < min ? `Must be at least ${min}` : undefined;

const minValue1 = minValue(1);

const maxValue = max => value =>
    value && value > max ? `Must be less ${max}` : undefined;

const maxValue31 = maxValue(31);
const maxValue12 = maxValue(12);

const length = len => value =>
    value.length!== len ? `Must be ${len} characters` : undefined;

const length4 = length(4);
const length2 = length(2);




const dayOfBirth = ({ input, label, type, meta: { touched, error, warning } }) => (
    <div>
        <div>
            <label className="step2-label">
                <input {...input} type={type} placeholder={label}/>
            </label>
            {touched && ((error && <span className="error">{error}</span>) || (warning && <span className="warn">{warning}</span>))}
        </div>
    </div>
);

const renderField = ({ input, label, type, check, meta: { touched, error,warning } }) => (
    <div>
        <div className="wrap-radio">
            <label className="step2-label radio">
                <input {...input} type={type} checked={check}/>
                <span>{label} </span>
            </label>
            {touched && ((error && <span className="error">{error}</span>) || (warning && <span className="warn">{warning}</span>))}
        </div>
    </div>
);

const optionField = ({ input, label,  meta: { touched, error, warning} }) => (
    <div>
        <div>
            <label className="step2-label option">{label}
                <select {...input}>
                  <option value=""></option>
                  <option value="net">Net</option>
                  <option value="magazine">Magazine</option>
                  <option value="TV">TV</option>
                </select>
            </label>
            {touched && ((error && <span className="error">{error}</span>) || (warning && <span className="warn">{warning}</span>))}
        </div>
    </div>
);

class Step2 extends Component {

  render() {
    const {handleSubmit, check} = this.props;


    return (
          <div className="step2" >
             <div className="title">
                <h1>Sign up</h1>
                <div className="progress-bar step2"></div>
             </div>
             <form onSubmit={handleSubmit}>
                <div className="date-of-birth">
                    <h2>DATE OF BIRTH</h2>
                    <div className="wrap-field">
                        <Field name="day"
                               component={dayOfBirth}
                               type="text"
                               label="DD"
                               validate={[required, minValue1, maxValue31, number, length2 ]}
                               />
                        <Field name="month"
                               component={dayOfBirth}
                               checked={check}
                               type="text"
                               label="MM"
                               validate={[required, minValue1, maxValue12, number, length2]}
                               />
                        <Field name="year"
                               component={dayOfBirth}
                               type="text"
                               label="YYYY"
                               validate={[required, number, length4]}
                               />
                    </div>

                </div>
                <div className="type-of-sex">
                    <h2>GENDER</h2>
                    <div className="wrap-type-of-sex">
                        <Field name="sex"
                               component={renderField}
                               type="radio"
                               label="Male"
                               value="male"/>
                        <Field name="sex"
                               component={renderField}
                               type="radio"
                               label="Female"
                               value="female"/>
                        <Field name="sex"
                               component={renderField}
                               type="radio"
                               label="Unspecified"
                               value="unspecified"/>
                    </div>
                </div>
                <div className="hear-about">
                    <h2>WHERE DID YOU HEAR ABOUT IS?</h2>
                    <Field name="hearAbout"
                           component={optionField}
                           type="select"
                           />
                </div>
                 <div className="footer">
                     <button className="btn-back" onClick={this.props.back}>Back</button>
                    <button type="submit">Next</button>
                 </div>
             </form>

          </div>

    );
  }
}


Step2 = reduxForm({
  form: 'test-form2',
  destroyOnUnmount: false,
    validate
})(Step2);

Step2 = connect(
    state =>( {
        check:state.account.data
    }  )  )(Step2);

export default Step2;
